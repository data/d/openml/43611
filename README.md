# OpenML dataset: Wisconsin-breast-cancer-cytology-features

https://www.openml.org/d/43611

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Cytology features of breast cancer biopsy. It can be used to predict breast cancer from cytology features.
The data was obtained from https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Original) 
Data description can be found at https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/breast-cancer-wisconsin.names
Content
Data contains cytology features of breast cancer biopsies - clump thickness, uniformity of cell size, uniformity of cell shape, marginal adhesion, single epithelial cell size, bare nuclei, bland chromatin, normal nuceloli, mitosis. The class variable denotes whether it was cancer or not. Cancer = 1 and not cancer = 0
Attribute Information:

Sample code number: id number 
Clump Thickness: 1 - 10 
Uniformity of Cell Size: 1 - 10 
Uniformity of Cell Shape: 1 - 10 
Marginal Adhesion: 1 - 10 
Single Epithelial Cell Size: 1 - 10 
Bare Nuclei: 1 - 10 
Bland Chromatin: 1 - 10 
Normal Nucleoli: 1 - 10 
Mitoses: 1 - 10 
Class: (0 for benign, 1 for malignant)

Acknowledgements
Data obtained from : UCI machine learning repository 
Dua, D. and Karra Taniskidou, E. (2017). UCI Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School of Information and Computer Science.
Picture courtesy: Photo by Pablo Heimplatz on Unsplash

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43611) of an [OpenML dataset](https://www.openml.org/d/43611). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43611/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43611/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43611/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

